//
//  PhotoModel.swift
//  PhotosApp
//
//  Created by Vladimir on 14.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

struct Photo: Codable {
    var title: String?
    var photoUrl: String?
    var phootId: String?
    var publishedAt: String?
    var comment: String?
    var photo: UIImage?
    
    enum CodingKeys: String, CodingKey {
        case title, publishedAt, comment, photoUrl = "picture", phootId = "_id"
    }
}
