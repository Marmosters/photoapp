//
//  PhotoNetworkService.swift
//  PhotosApp
//
//  Created by Vladimir on 14.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

protocol PhotoNetworkService {
    var apiUrl: String {get}
    func loadData(completion: (([Photo]?, Error?) -> Void)?)
}

extension PhotoNetworkService {
    
    func loadData(completion: (([Photo]?, Error?) -> Void)?) {
        let session = URLSession.shared
        guard  let url = URL(string: apiUrl) else {
            completion?(nil, nil)
            return
        }
        
        let task = session.dataTask(with: url) { data, response, error in
            guard  error == nil, let data = data else {
                completion?(nil, error)
                return
            }
            guard let response = response, let mime = response.mimeType, mime == "application/json" else {
                let error = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: PAStrings.wrongMIMETypeError])
                completion?(nil, error)
                return
            }
            do {
                var result = try JSONDecoder().decode([Photo].self, from: data)
                let group = DispatchGroup()
                result.indices.forEach({ index in
                    group.enter()
                    self.loadImage(photo: &result[index]) { image in
                        result[index].photo = image
                        group.leave()
                    }
                })
                group.wait()
                completion?(result, nil)
            } catch {
                completion?(nil, error)
            }
        }
        task.resume()
    }
    
    func loadImage(photo: inout Photo, completion: ((UIImage?) -> Void)?) {
        guard let photoUrl = photo.photoUrl, let url = URL(string: photoUrl) else {
            completion?(nil)
            return
        }
        var image: UIImage?
        DispatchQueue.global(qos: .userInitiated).async {
            if let data = try? Data(contentsOf: url) {
                image = UIImage(data: data)
            }
            completion?(image)
        }
    }
}
