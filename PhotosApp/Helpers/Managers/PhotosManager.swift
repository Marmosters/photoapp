//
//  PhotosManager.swift
//  PhotosApp
//
//  Created by Vladimir on 15.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import Foundation

class PhotosManager: PhotoNetworkService {
    
    let apiUrl = "http://www.json-generator.com/api/json/get/cftPFNNHsi"
    
    func getPhotos(completion: (([Photo]?, Error?) -> Void)?) {
        DispatchQueue.global(qos: .userInitiated).async {
            self.loadData { photos, error in
                DispatchQueue.main.async {
                    completion?(photos, error)
                }
            }
        }
    }
}
