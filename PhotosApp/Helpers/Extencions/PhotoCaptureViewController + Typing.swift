//
//  PhotoCaptureViewController + Typing.swift
//  PhotosApp
//
//  Created by Vladimir. Evstratov on 12/18/19.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

// MARK: Keyboard avoidence
extension PhotoCaptureViewController {
    var bottomEdgeInsets: CGFloat {
        let window = UIApplication.shared.windows.first
        if #available(iOS 11.0, *) {
            return  window?.safeAreaInsets.bottom ?? 0
        } else {
            return 0
        }
    }
    
    @objc func onKeyboardFrameChange(notification: NSNotification) {
        guard let userInfo = notification.userInfo else {return}
        let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
        let endFrameY = endFrame?.origin.y ?? 0
        if endFrameY >= UIScreen.main.bounds.size.height {
            self.bottomButtonsViewHeightConstraint.constant = 0.0
        } else {
            self.bottomButtonsViewHeightConstraint.constant = endFrame?.size.height ?? 0.0
            if #available(iOS 11.0, *), self.bottomButtonsViewHeightConstraint.constant > 0 {
    
                let window = UIApplication.shared.windows.first
                 self.bottomButtonsViewHeightConstraint.constant -= window?.safeAreaInsets.bottom ?? 0
            }
        }
        let duration: TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve: UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
        UIView.animate(withDuration: duration,
                                   delay: TimeInterval(0),
                                   options: animationCurve,
                                   animations: { self.view.layoutIfNeeded() },
                                   completion: nil)
       
    }
    
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}

extension PhotoCaptureViewController: UITextFieldDelegate {

}
