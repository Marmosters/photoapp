//
//  CGFrame + Raito.swift
//  PhotosApp
//
//  Created by Vladimir. Evstratov on 12/18/19.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

extension UIImage {
    var raito: CGFloat {
        return self.size.height / self.size.width
    }
}
