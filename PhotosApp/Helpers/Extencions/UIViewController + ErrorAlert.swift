//
//  UIViewController + ErrorAlert.swift
//  PhotosApp
//
//  Created by Vladimir on 19.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

extension UIViewController {
    func showError(_ errorText: String, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: PAStrings.errorTitle, message: errorText, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: PAStrings.labelOk, style: UIAlertAction.Style.default, handler: {_ in completion?()}))
        self.present(alert, animated: true, completion: nil)
    }
}
