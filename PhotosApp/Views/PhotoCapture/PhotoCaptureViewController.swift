//
//  PhotoCaptureViewController.swift
//  PhotosApp
//
//  Created by Vladimir. Evstratov on 12/16/19.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit
import AVFoundation

protocol PhotoCaptureDelegate: class {
    func photoDidCaptured(photo: Photo)
}

class PhotoCaptureViewController: UIViewController {
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var descriptionTextField: UITextField!
    @IBOutlet private weak var captureButton: UIButton!
    @IBOutlet private weak var captureButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var bottomButtonsView: UIView!
    @IBOutlet weak var bottomButtonsViewHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: PhotoCaptureDelegate?
    var photoOrientaion: UIImage.Orientation!
    var photo: UIImage? {
        didSet {
            photoImageView.image = photo
        }
    }
    
    private let cameraMediaType = AVMediaType.video
    private var cameraAuthorizationStatus: AVAuthorizationStatus {
        return AVCaptureDevice.authorizationStatus(for: cameraMediaType)
    }

    private var session: AVCaptureSession?
    private var cameraOutput: AVCapturePhotoOutput?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTypingView()
        setupCamera()
        session?.startRunning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //guard cameraAuthorizationStatus == .authorized else {return}
        startCapture()
    }
        
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        updateOrientation(deviceOrientation: toInterfaceOrientation)
    }
        
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        videoPreviewLayer?.frame = view.bounds
    }
    
    private func setupTypingView() {
        descriptionTextField.delegate = self
        descriptionTextField.returnKeyType = .done
        descriptionTextField.addTarget(self, action: #selector(doneButtonAction), for: UIControl.Event.editingDidEndOnExit)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardFrameChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardFrameChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        hideKeyboardWhenTappedAround()
        bottomButtonsViewHeightConstraint.constant += bottomEdgeInsets
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func captureButtonAction(_ sender: Any) {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        settings.previewPhotoFormat = previewFormat
        cameraOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        descriptionTextField.resignFirstResponder()
        descriptionTextField.text = nil
        startCapture()
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        dismiss(animated: true) {
            var photo = Photo()
            photo.title =  self.descriptionTextField.text
            photo.photo = self.photo
            self.delegate?.photoDidCaptured(photo: photo)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: Camera stuff
extension PhotoCaptureViewController: AVCapturePhotoCaptureDelegate {
    // swiftlint:disable:next function_parameter_count line_length
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        stopCapture()
        
        if let error = error {
            showError(error.localizedDescription)
        }

        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer,
            let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {

            let dataProvider = CGDataProvider(data: dataImage as CFData)
            let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            photo = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: photoOrientaion)
         } else {
            showError(PAStrings.unknownError)
        }
    }
    
    private func setupCamera() {
        session = AVCaptureSession()
        session?.sessionPreset = AVCaptureSession.Preset.photo
        guard let backCamera =  AVCaptureDevice.default(for: AVMediaType.video) else {return}
        guard let input = try? AVCaptureDeviceInput(device: backCamera), session?.canAddInput(input) ?? false else {return}
        session?.addInput(input)
        cameraOutput = AVCapturePhotoOutput()
        if session!.canAddOutput(cameraOutput!) {
            session!.addOutput(cameraOutput!)
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
            videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
            updateOrientation(deviceOrientation: UIApplication.shared.statusBarOrientation)
        }

    }
    
    private func startCapture() {
        guard cameraAuthorizationStatus == .authorized  || cameraAuthorizationStatus == .notDetermined else {
            dismissWithCameraError()
            return
        }
        session?.startRunning()
        captureButton.isHidden = false
        bottomButtonsView.isHidden = true
        view.layer.addSublayer(videoPreviewLayer!)
        videoPreviewLayer?.frame = view.bounds
        view.bringSubviewToFront(closeButton)
        view.bringSubviewToFront(captureButton)
        
    }
    
    private func stopCapture() {
        session?.stopRunning()
        videoPreviewLayer?.removeFromSuperlayer()
        captureButton.isHidden = true
        bottomButtonsView.isHidden = false
    }
    
    private func dismissWithCameraError() {
        var error = ""
        switch cameraAuthorizationStatus {
        case .authorized, .notDetermined: break
        case .denied:
            error =  PAStrings.cameraDenided
        case .restricted:
            error = PAStrings.cameraRestricted
        default:
            error = PAStrings.unknownError
        }
        showError(error) {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: Other
extension PhotoCaptureViewController {
    private func updateOrientation(deviceOrientation: UIInterfaceOrientation) {
        var orientation: AVCaptureVideoOrientation
        switch deviceOrientation {
        case .portraitUpsideDown:
            orientation = .portraitUpsideDown
            photoImageView.contentMode = .scaleAspectFill
            photoOrientaion = .left
        case .landscapeLeft:
            orientation = .landscapeLeft
            photoImageView.contentMode = .scaleAspectFit
            photoOrientaion = .down
        case .landscapeRight:
            orientation = .landscapeRight
            photoImageView.contentMode = .scaleAspectFit
            photoOrientaion = .up
        default:
            orientation = .portrait
            photoImageView.contentMode = .scaleAspectFill
            photoOrientaion = .right
        }
        videoPreviewLayer?.connection?.videoOrientation = orientation
    }
}
