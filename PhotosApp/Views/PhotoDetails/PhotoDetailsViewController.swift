//
//  PhotoDetailsViewController.swift
//  PhotosApp
//
//  Created by Vladimir. Evstratov on 12/16/19.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

class PhotoDetailsViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    var photo: Photo?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        image.image = photo?.photo
        descriptionLabel.text = photo?.comment
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateConstraints()
    }
    
    
    private func setupNavigationBar() {
        let rightButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(rightButtonPressd(sender:)))
        navigationItem.rightBarButtonItem = rightButton
        title = photo?.title
    }
    
    @objc private func rightButtonPressd(sender: UIBarButtonItem) {
        guard let imageToShare = image.image else {return}
        let activityController = UIActivityViewController(activityItems: [imageToShare], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view
        self.present(activityController, animated: true, completion: nil)
    }
    
    private func updateConstraints() {
        guard let photoImage = photo?.photo else {return}
        imageWidthConstraint.constant = contentView.frame.width - 16
        imageHeightConstraint.constant = imageWidthConstraint.constant * photoImage.raito
        contentView.layoutIfNeeded()
        scrollView.contentSize = contentView.frame.size
        view.layoutIfNeeded()
        
    }

}
