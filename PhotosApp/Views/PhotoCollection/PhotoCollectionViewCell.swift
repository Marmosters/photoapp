//
//  PhotoCollectionViewCell.swift
//  PhotosApp
//
//  Created by Vladimir on 15.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photo: UIImageView!

    var model: Photo? {
        didSet {
            photo.image = model?.photo
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        model = nil
    }

}
