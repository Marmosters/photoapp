//
//  PhotosCollectionViewController.swift
//  PhotosApp
//
//  Created by Vladimir on 14.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

class PhotosCollectionViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let presenter = PhotosPresenter()
    private let loadingView = LoadingView(frame: .zero)
    
    private var layout: PhotoLayout? {
        return collectionView?.collectionViewLayout as? PhotoLayout
    }
    
    var rightButton: UIBarButtonItem?
    var topLeftCollectionItem: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupCollectionView()
        presenter.delegate = self
        presenter.getPhotos()
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        let visibleCells = collectionView.visibleCells.sorted { $0.tag < $1.tag}
        topLeftCollectionItem = visibleCells.first?.tag ?? 0
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        let firstIndexPath = IndexPath(item: topLeftCollectionItem ?? 0, section: 0)
        collectionView.scrollToItem(at: firstIndexPath, at: .top, animated: true)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func setupCollectionView() {
        self.collectionView.register(UINib(nibName: PANibs.photoCellNIB, bundle: nil), forCellWithReuseIdentifier: PAReuseIdentifers.photoCellIdentifier)
        
        if let layout = layout {
            layout.delegate = self
        }
        automaticallyAdjustsScrollViewInsets = false
    }
    
    private func setupNavigationBar() {
        rightButton = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(rightButtonPressd(sender:)))
        navigationItem.rightBarButtonItem = rightButton
        title = PAStrings.mainTitle
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let model = sender as? Photo, let vc = segue.destination as? PhotoDetailsViewController {
            vc.photo = model
        }
    }
    
    @objc private func rightButtonPressd(sender: UIBarButtonItem) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let capturePhotoVC = PhotoCaptureViewController(nibName: PANibs.photoDescriptionVCNIB, bundle: nil)
            capturePhotoVC.modalPresentationStyle = .fullScreen
            capturePhotoVC.delegate = presenter
            self.present(capturePhotoVC, animated: true, completion: nil)
        }
    }
}

// MARK: UICollectionViewDataSource
extension PhotosCollectionViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfPhotos
    }

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PAReuseIdentifers.photoCellIdentifier, for: indexPath)
        if let photoCell = cell as? PhotoCollectionViewCell {
            photoCell.model = presenter.modelForRow(at: indexPath)
            photoCell.tag = indexPath.row
        }
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension PhotosCollectionViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let model = presenter.modelForRow(at: indexPath) else {return}
        performSegue(withIdentifier: PASegueIdentifers.photoDetailsSegue, sender: model)
    }
}
  

// MARK: PhotoLayoutDelelate
extension PhotosCollectionViewController: PhotoLayoutDelelate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAt indexPath: IndexPath) -> CGFloat {
        guard let layout = layout, let photo = presenter.modelForRow(at: indexPath)?.photo else {return 0.0}
        return photo.raito * layout.columnWidth
    }
}

// MARK: PhotoPresenterDelegate
extension PhotosCollectionViewController: PhotoPresenterDelegate {
    func photosDidUpdated() {
        collectionView.reloadData()
        rightButton?.isEnabled = true
        loadingView.stopSpinner()
    }
    
    func photosUpdateWillStart() {
        view.addSubview(loadingView)
        loadingView.frame = view.bounds
        rightButton?.isEnabled = false
        loadingView.startSpinner()
    }
}
