//
//  PhotoAppStrings.swift
//  PhotosApp
//
//  Created by Vladimir on 15.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import Foundation

class PAStrings {
    static let unknownError = NSLocalizedString("UnknownError", comment: "")
    static let wrongMIMETypeError = NSLocalizedString("WrongMIMETypeError", comment: "")
    static let mainTitle = NSLocalizedString("MainTitle", comment: "")
    static let errorTitle = NSLocalizedString("ErrorTitle", comment: "")
    static let labelOk = NSLocalizedString("LabelOK", comment: "")
    static let cameraDenided = NSLocalizedString("CameraDenided", comment: "")
    static let cameraRestricted = NSLocalizedString("CameraRestricted", comment: "")
}
