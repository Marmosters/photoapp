//
//  PAConstants.swift
//  PhotosApp
//
//  Created by Vladimir on 15.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import Foundation

class PAReuseIdentifers {
    static let photoCellIdentifier = "photoCell"
}

class PANibs {
    static let photoCellNIB = "PhotoCollectionViewCell"
    static let photoDescriptionVCNIB = "PhotoCaptureViewController"
}

class PASegueIdentifers {
    static let photoDetailsSegue = "photoDetailsSegue"
}
