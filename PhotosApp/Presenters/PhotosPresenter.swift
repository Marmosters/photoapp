//
//  PhotosPresenter.swift
//  PhotosApp
//
//  Created by Vladimir on 14.12.2019.
//  Copyright © 2019 Vladimir. All rights reserved.
//

import UIKit

protocol PhotoPresenterDelegate: class {
    func photosUpdateWillStart()
    func photosDidUpdated()
    func showError(_ errorText: String, completion: (() -> Void)?)
}

class PhotosPresenter {

    private let manager = PhotosManager()
    weak var delegate: PhotoPresenterDelegate?
    private var photos = [Photo]()
    
    var numberOfPhotos: Int {
        return photos.count
    }
    
    func modelForRow(at indexPath: IndexPath) -> Photo? {
        return photos.indices.contains(indexPath.row) ? photos[indexPath.row] : nil
    }
    
    func getPhotos() {
        delegate?.photosUpdateWillStart()
        manager.getPhotos {[weak self] photos, error in
            guard let photos = photos else {
                self?.delegate?.showError(error?.localizedDescription ?? PAStrings.unknownError, completion: nil)
                return
            }
            self?.photos = photos
            self?.delegate?.photosDidUpdated()
        }
    }
}

// MARK: PhotoCaptureDelegate
extension PhotosPresenter: PhotoCaptureDelegate {
    func photoDidCaptured(photo: Photo) {
        photos.append(photo)
        delegate?.photosDidUpdated()
    }
    
    
}
