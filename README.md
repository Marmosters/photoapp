**iOS interview project**

This is an interview project. The goal is to create an iOS app that displays photo gallery downloaded from the web. The app has to be smooth, without glitches while scrolling, stable and within Apple's Human Interface Guidlines.

**Requirements:**
* One screen that displays all the photos.
* The photos should be loaded from a webservice that returns a json document. The URL of the endpoint is: http://www.json-generator.com/api/json/get/cftPFNNHsi
* When user taps onto some photo, another screen should be presented with only the photo, description and a share button.
* Share button should open standard system share with the photo as an argument.
* App should support both landscape and portrait orientation.
* In the first screen, app should have a button which would allow user to take a photo with the camera and display it in the photo list along with the other photos.
* We need the project to support iOS 10+.
* The use of third party dependencies are not allowed.
